(function($) {

    $.fn.greenify = function() {
        this.css("color", "green");
        return this;
    };

}(jQuery));

//# sourceMappingURL=plugin.js.map